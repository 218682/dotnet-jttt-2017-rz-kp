﻿using HtmlAgilityPack;
using System;
using System.Net;
using System.Text;
using System.Collections.Generic;
namespace DownloadNodesSample
{
    public class HtmlPage
    {
        private readonly string _url;

        public HtmlPage(string url)
        {
            this._url = url;
        }

        public string GetPageHtml()
        {
            using (var wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                var html = System.Net.WebUtility.HtmlDecode(wc.DownloadString(_url));

                return html;
            }
        }
        public void PrintPageNodes()
        {
            var doc = new HtmlDocument();

            var pageHtml = GetPageHtml();

            doc.LoadHtml(pageHtml);

            var nodes = doc.DocumentNode.Descendants("img");

            foreach (var node in nodes)
            {
                Console.WriteLine("---------");
                Console.WriteLine("Node name: " + node.Name);
                Console.WriteLine("Src value: " + node.GetAttributeValue("src", ""));
                Console.WriteLine("Alt value: " + node.GetAttributeValue("alt", ""));
            }
        }
        /// <summary>
        /// find IMG on url and return it src
        /// </summary>
        /// <param name="words"> - words to find on given url</param>
        /// <param name="type"> - type of node to check</param>
        /// <returns> src of image or null if given world don't match to any of images</returns>
        public String find(List<string> words, string type="img")
        {
            var doc = new HtmlDocument();
            var pageHtml = GetPageHtml();

            doc.LoadHtml(pageHtml);
            var nodes = doc.DocumentNode.Descendants(type);
                foreach (var node in nodes)
                {
                    int count = 0;
                    foreach(string word in words)
                    {
                        if (node.GetAttributeValue("alt", "").Contains(word))
                            count++;
                    }
                    if(count==words.Count)
                        return node.GetAttributeValue("src", "");
                }
            
            return null;
        }
    }
}
