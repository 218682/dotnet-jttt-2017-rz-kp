﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JTTT
{
    public partial class JTTT : Form
    {
        BindingList<FAEmail> listaZadan = new BindingList<FAEmail>();
        public JTTT()
        {
            InitializeComponent();
        }

        private void RUN_Click(object sender, EventArgs e)
        {
            FAEmail zadanie;
            if (word.Text != "")
            {
                if (fromPage.Text != "")
                {
                    if (mail.Text != "")
                    {
                        String name = taskName.Text;
                        String from;
                        if (fromPage.Text.Contains("http://"))
                            from = fromPage.Text;
                        else
                            from = "http://" + fromPage.Text;
                       
                        List<String> words = new List<String>();
                        words.Add(word.Text);
                        zadanie = new FAEmail(from, mail.Text, words, name);
                        //zadanie.findIMG(); // <- zamiast tego wysyłanie utworzonego zadania na liste zadań
                        listaZadan.Add(zadanie);
                        jobsList.DataSource = listaZadan;
                    }
                }                
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {
           
        }

        private void JTTT_Load(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            listaZadan.Clear();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            foreach(FAEmail job in listaZadan)
            {
                job.findIMG();
            }
        }

        private void label8_Click(object sender, EventArgs e)
        {
          
        }

        private void taskName_TextChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            serialize(listaZadan);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            deSerialize(listaZadan);
        }
        private void serialize(BindingList<FAEmail> listaZadan)
        {
            Hashtable tablicaKeyValue = new Hashtable();
            foreach (FAEmail zadaniePom in listaZadan)
            {
                tablicaKeyValue.Add("Task", zadaniePom.taskName);
                tablicaKeyValue.Add("Adres", zadaniePom.mailTo);
                tablicaKeyValue.Add("Url", zadaniePom.url);
                int counter = 0;
                foreach (string word in zadaniePom.wordsToFind)
                {
                    tablicaKeyValue.Add("Word" + counter.ToString(), zadaniePom.wordsToFind[counter]);
                }
            }
    
            FileStream fs = new FileStream("zadania.dat", FileMode.OpenOrCreate, FileAccess.ReadWrite);
            BinaryFormatter formatter = new BinaryFormatter();
            try
            {
                formatter.Serialize(fs, tablicaKeyValue);
            }
            catch (SerializationException e)
            {
                Console.WriteLine("Failed to serialize. Reason: " + e.Message);
                throw;
            }
            finally
            {
                fs.Close();
            }
        }

        public void deSerialize(BindingList<FAEmail> listaZadan)
        {
            Hashtable tablicaKeyValue = null;
            FileStream fs = new FileStream("zadania.dat", FileMode.Open, FileAccess.ReadWrite);
            try
            {
                BinaryFormatter formatter = new BinaryFormatter();
                // Deserialize the hashtable from the file and 
                // assign the reference to the local variable.
                tablicaKeyValue = (Hashtable)formatter.Deserialize(fs);
            }
            catch (SerializationException e)
            {
                Console.WriteLine("Failed to deserialize. Reason: " + e.Message);
                throw;
            }
            finally
            {
                fs.Close();
            }

            int counter = 0;
            foreach (DictionaryEntry pair in tablicaKeyValue)
            {
                if (pair.Key.Equals("Task"))
                    counter++;
            }

            for (int i = 0; i < counter; i++)
            {
                String pomMailTo ="", pomUrl="", pomTaskName="";
                List<String> pomWords = new List<String>();
                bool czyJuzBylTenTask = false;
                foreach (DictionaryEntry pair in tablicaKeyValue)
                {
                    if (pair.Key.Equals("Task"))
                    {
                        if (czyJuzBylTenTask == true)
                        {
                            FAEmail pomJob = new FAEmail(pomUrl, pomMailTo, pomWords, pomTaskName);
                            listaZadan.Add(pomJob);
                            break;
                        }                           
                        pomTaskName = (String)pair.Value;
                        czyJuzBylTenTask = true;
                    }
                    else if (pair.Key.Equals("Adres"))
                        pomMailTo = (String)pair.Value;
                    else if (pair.Key.Equals("Url"))
                        pomUrl = (String)pair.Value;
                    else
                    {
                        pomWords.Add((String)pair.Value);
                    }
                    tablicaKeyValue.Remove(pair);
                }
            }
            jobsList.DataSource = listaZadan;
        }

        private void label4_Click_1(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }
    }
}
