﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DownloadNodesSample;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace JTTT
{
    /// <summary>
    /// Find And Email information
    /// </summary>
    public class FAEmail : IManager
    {
        public String taskName;
        public String url;
        public String mailTo;
        public List<String> wordsToFind;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"> url of internet page to look for specified words </param>
        /// <param name="mail"> mail adrres to send information                 </param>
        /// <param name="words"> words that have to be in img description       </param>
        public FAEmail(String url, String mail, List<String> words, String taskName)
        {   
            this.url = url;
            this.mailTo = mail;
            wordsToFind = words;
            this.taskName = taskName;
        }
        /// <summary>
        /// find specified in constructor img, write it url to log and send it by email
        /// </summary>
        /// <returns> string that can be displeyed as information of succes or failure </returns>
        public string findIMG()
        {
            
            DateTime localDate = DateTime.Now;
            HtmlPage pageToSearch = new HtmlPage(url);
            String imageURL= pageToSearch.find(wordsToFind);
            if (imageURL != null)
            {
                using (System.IO.StreamWriter file = new System.IO.StreamWriter("jttt.log", true))
                {
                    //write log
                    Console.WriteLine("Znaleziono: " + url + imageURL);
                    file.WriteLine(localDate.ToString() + " " + url + imageURL);
                    //send mail
                    if (mailTo != "")
                    {
                        Email email = new Email(mailTo);
                        if(email.send(url + imageURL))
                            return "Email sent";
                    }
                    return "Ther is a match, but email wasn't sent";

                }
            }
            else
            {
                Console.WriteLine("Nic nie znaleziono, dostępne obrazki: ");
                pageToSearch.PrintPageNodes();
                using (System.IO.StreamWriter file = new System.IO.StreamWriter("jttt.log", true))
                {
                    //write log
                    file.WriteLine(localDate.ToString() + " Nic nie znaleziono");
                }
                return "Nothing match";
            }
        }
    }
}
