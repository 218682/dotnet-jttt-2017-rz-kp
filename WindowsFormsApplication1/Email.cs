﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Net.Mime;
using System.Net;
using System.Security;

namespace JTTT
{
    class Email
    {
        private MailAddress mailTo;
        private MailAddress mailFrom;
        bool mailSent = true;
        public Email(String mail)
        {
            mailFrom = new MailAddress("dotnet.jttt.2017.RZ.KP@gmail.com", "JTTTapp");
            mailTo = new MailAddress(mail);

        }
        public bool send(String text, String to=null, String from=null)
        {

            if (from != null)
            {
                mailFrom = new MailAddress(from);
            }
            if (to != null)
                mailTo = new MailAddress(to);

            //---------------- Smpt configuration ----------------------------------//
            SmtpClient client = new SmtpClient("smtp.gmail.com");
            client.UseDefaultCredentials = false;
            client.EnableSsl = true;
            client.Port =  587;
            client.Credentials = new NetworkCredential("dotnet.jttt.2017.RZ.KP@gmail.com", "dotnet2017");
            //----------------------------------------------------------------------//

            //---------------- Message configuration -------------------------------//
            MailMessage message = new MailMessage(mailFrom, mailTo);
            message.Body = (text);
            message.Subject = "JTTT newsleter";
            //----------------------------------------------------------------------//

            Console.WriteLine("Sending an e-mail message to {0} at {1} by using the SMTP host={2}.",mailTo.User, mailTo.Host, client.Host);
            try
            {
                client.Send(message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception caught in CreateTestMessage3(): {0}",
                            ex.ToString());
                mailSent = false;
            }


            message.Dispose();
            client.Dispose();
            if (mailSent == true)
            {
                Console.WriteLine("Message sent");
                return false;
            }
            else
            {
                Console.WriteLine("Error, Message wasn't sent");
                return true;
            }
        }
    }
}
